# Open edX for KU

Open edX for KU is a Docker-based Open edX modified for KU Learn, Kasetsart
University's MOOC platform.  It is built on top of
[overhangio/openedx](https://hub.docker.com/r/overhangio/openedx/) Docker image
used by [Tutor Open edX distribution](https://docs.tutor.overhang.io/).
