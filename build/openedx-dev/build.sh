#!/bin/sh

VERSION=`cat ../../VERSION`
IMAGE=cjaikaeo/openedx-ku-dev:$VERSION

if [ -d patch ]; then
    tar zcf ku-patch.tgz -C patch .
fi

docker build --build-arg version=$VERSION -t $IMAGE .

rm -f ku-patch.tgz
