#!/bin/sh

VERSION=`cat ../../VERSION`
IMAGE=cjaikaeo/openedx-ku:$VERSION

if [ -d patch ]; then
    tar zcf ku-patch.tgz -C patch .
fi

docker build -t $IMAGE .

rm -f ku-patch.tgz
