#!/bin/sh

if [ -z $1 ]; then
    echo "Usage: $0 <container-id>"
    exit 1
fi

docker cp patch $1:/
docker exec $1 sh -c "cp -a /patch/* /openedx/edx-platform"
