from celery import task
from celery.utils.log import get_task_logger

from django.contrib.auth.models import User
from course_creators.models import CourseCreator
from course_creators.views import add_user_with_status_granted
from addons.kulearn.common.utils import is_ku_personnel

LOG_PREFIX = "KU Learn"

LOGGER = get_task_logger(__name__)

@task(ignore_result=True)
def approve_course_creators():
    admin = User.objects.filter(is_superuser=True).first()
    pendings = CourseCreator.objects.filter(state=CourseCreator.PENDING)
    for req in pendings:
        user = req.user
        req.delete()
        if is_ku_personnel(user):
            add_user_with_status_granted(admin, user)
            LOGGER.info('[{}] Granted course creator rights to {}'.format(
                LOG_PREFIX, user.username))
        else:
            LOGGER.info('[{}] Denied course creator rights for {}'.format(
                LOG_PREFIX, user.username))
