from django.contrib.auth.models import User
from social_django.models import UserSocialAuth

def is_ku_personnel(user):
    """Check if the user belongs to the type Personnel based Google People's
    organizational data stored during registration"""

    try:
        social = user.social_auth.get(provider="google-oauth2")
        data = social.extra_data
        return social.extra_data['organizations'][0]['title'] == 'Personnel'
    except UserSocialAuth.DoesNotExist:
        return False
    except KeyError:
        return False

def _delete_user(username):
    """Internal use for convenient removal of a user"""
    user = User.objects.get(username=username)
    user.social_auth.all().delete()
    user.delete()
