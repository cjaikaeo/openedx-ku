import os
from django.views import View
from django.http import Http404, HttpResponse
from django.views.generic import TemplateView

###########################################################
# Adapted from https://gist.github.com/MhmdRyhn/820e3277c157098655dd3ae117be4285
class FileDownloadView(View):
    file_path = ''
    content_type_value = 'text/plain'

    def get(self, request):
        if os.path.exists(self.file_path):
            with open(self.file_path, 'rb') as fh:
                response = HttpResponse(
                    fh.read(),
                    content_type=self.content_type_value
                )
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(self.file_path)
            return response
        else:
            raise Http404

###########################################################
class SwaggerView(TemplateView):
    template_name = "addons/kulearn/common/swagger-ui.html"
    schema_url = ""

    def get_context_data(self, *args, **kwargs):
        context = super(SwaggerView,self).get_context_data(*args, **kwargs)
        context["schema_url"] = self.schema_url
        return context

