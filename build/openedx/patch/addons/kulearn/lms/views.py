import os
import json
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.contrib.auth.models import User

from rest_framework import views, status, generics, mixins, schemas, decorators
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from edx_rest_framework_extensions.auth.jwt.authentication import JwtAuthentication
from openedx.core.djangoapps.content.course_overviews.models import CourseOverview
from openedx.core.djangoapps.user_api.serializers import UserSerializer
from openedx.core.lib.api.permissions import ApiKeyHeaderPermission, ApiKeyHeaderPermissionIsAuthenticated
from .serializers import (
    EnrollmentSerializer,
    EnrollmentDetailSerializer,
)
from opaque_keys.edx.keys import CourseKey
from student.models import CourseEnrollment
import enrollment as edx_enrollment

from addons.kulearn.common.views import (
    FileDownloadView,
    SwaggerView as CommonSwaggerView,
)

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))

###########################################################
class SchemaView(FileDownloadView):
    file_path = os.path.join(CURRENT_DIR, 'openapi', 'schema.yaml')

###########################################################
class SwaggerView(CommonSwaggerView):
    file_path = os.path.join(CURRENT_DIR, 'openapi', 'schema.yaml')
    schema_url = reverse_lazy('schema_view')

###########################################################
class EnrollmentListView(mixins.ListModelMixin,
                         mixins.CreateModelMixin,
                         edx_enrollment.views.ApiKeyPermissionMixIn,
                         generics.GenericAPIView):

    serializer_class = EnrollmentSerializer
    authentication_classes = [JwtAuthentication]
    permission_classes = [IsAdminUser, ApiKeyHeaderPermissionIsAuthenticated]

    def get_queryset(self):
        course_key = CourseKey.from_string(self.kwargs['course_id'])
        return CourseEnrollment.objects.filter(course__id=course_key)

    #def get_serializer_context(self):
    #    context = super(EnrollmentListView,self).get_serializer_context()
    #    try:
    #        course_key = CourseKey.from_string(self.kwargs['course_id'])
    #        context['course'] = CourseOverview.objects.get(id=course_key)
    #    except KeyError:
    #        pass
    #    except ObjectDoesNotExist:
    #        raise Http404
    #    return context

    def get(self, request, *args, **kwargs):
        """Obtain a list of enrollments for the course
        """
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Create a new enrollment for the course
        """
        return self.create(request, *args, **kwargs)


###########################################################
class EnrollmentDetailView(mixins.DestroyModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.RetrieveModelMixin,
                           edx_enrollment.views.ApiKeyPermissionMixIn,
                           generics.GenericAPIView):

    serializer_class = EnrollmentDetailSerializer
    authentication_classes = [JwtAuthentication]
    permission_classes = [IsAdminUser, ApiKeyHeaderPermissionIsAuthenticated]

    def get_object(self):
        course_key = CourseKey.from_string(self.kwargs['course_id'])
        user = self.kwargs['username']
        obj = get_object_or_404(CourseEnrollment, course__id=course_key, user__username=user)
        return obj

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


###########################################################
class UserMapView(views.APIView):

    authentication_classes = [JwtAuthentication]
    permission_classes = [IsAdminUser, ApiKeyHeaderPermissionIsAuthenticated]

    def post(self, request):
        """
        Return a list of corresponding user names looked up from the specific email addresses
        """
        try:
            emails = request.data['emails']
            usermaps = []
            for email in emails:
                try:
                    user = User.objects.get(email=email)
                    username = user.username
                except User.DoesNotExist:
                    username = None
                usermaps.append({"email":email, "username":username})
            return Response(usermaps)
        except KeyError:
            return Response({"detail":"missing required parameter"}, status.HTTP_400_BAD_REQUEST)
        except Exception, e:
            return Response({"detail":str(e)}, status.HTTP_400_BAD_REQUEST)
