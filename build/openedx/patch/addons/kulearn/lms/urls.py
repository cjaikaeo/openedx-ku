import os
from django.conf import settings
from django.conf.urls import url
from django.contrib.admin.views.decorators import staff_member_required

from addons.kulearn.lms.views import (
    SchemaView,
    SwaggerView,
    EnrollmentListView,
    EnrollmentDetailView,
    UserMapView,
)

schema_view = staff_member_required(SchemaView.as_view())
swagger_view = staff_member_required(SwaggerView.as_view())
#schema_view = SchemaView.as_view()
#swagger_view = SwaggerView.as_view()

urlpatterns = [
    url(r'^openapi/$', swagger_view, name="swagger_view"),
    url(r'^openapi/schema.yaml$', schema_view, name="schema_view"),
    url(r'^api/v1/course/{}/enrollments$'.format(settings.COURSE_ID_PATTERN),
        EnrollmentListView.as_view(), name="enrollment_view"),
    url(r'^api/v1/course/{}/enrollment/{}$'.format(settings.COURSE_ID_PATTERN, settings.USERNAME_PATTERN),
        EnrollmentDetailView.as_view(), name="enrollment_detail"),
    url(r'^api/v1/usermap$',
        UserMapView.as_view(), name="usermap_view"),
]
