from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response
from student.models import CourseEnrollment
import enrollment as edx_enrollment

#class EnrollmentAttributeField(serializers.RelatedField):
#    def to_representation(self, obj):
#        return '{}:{},{}'.format(obj.namespace, obj.name, obj.value)
#
#
#class EnrollmentAttributeSerializer(serializers.ModelSerializer):
#    class Meta:
#        model = CourseEnrollmentAttribute
#        fields = ['namespace', 'name', 'value']


class EnrollmentSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(queryset=User.objects.all(), slug_field='username')

    class Meta:
        model = CourseEnrollment
        fields = ['user', 'created', 'is_active', 'mode']
        #validators = [
        #        UniqueTogetherValidator(
        #            queryset=CourseEnrollment.objects.all(),
        #            fields=('course','user'),
        #            message='User is allowed to enroll only once',
        #        )
        #    ]

    def create(self, validated_data):
        course_id = self.context['view'].kwargs['course_id']
        username = self.data['user']
        try:
            enrollment = edx_enrollment.api.add_enrollment(
                username,
                course_id,
                mode=validated_data['mode'],
                is_active=validated_data['is_active'],
            )
        #except edx_enrollment.errors.CourseEnrollmentExistsError:
        except Exception as e:
            return Response({'msg': str(e)}, status=status.HTTP_400_BAD_REQUEST)

        return enrollment


class EnrollmentDetailSerializer(serializers.ModelSerializer):
    #attributes = EnrollmentAttributeField(many=True, read_only=True)

    class Meta:
        model = CourseEnrollment
        #fields = ['created', 'is_active', 'mode', 'attributes']
        fields = ['created', 'is_active', 'mode']
